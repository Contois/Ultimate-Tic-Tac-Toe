
class Position:
    
    def __init__(self):
        self.board = []
        self.macroboard = []
    
    def parse_field(self, fstr):
        flist = fstr.replace(';', ',').split(',')
        self.board = [ int(f) for f in flist ]
    
    def parse_macroboard(self, mbstr):
        mblist = mbstr.replace(';', ',').split(',')
        self.macroboard = [ int(f) for f in mblist ]
    
    def is_legal(self, x, y):
        mbx, mby = x/3, y/3
        return self.macroboard[3*mby+mbx] == -1 and self.board[9*y+x] == 0

    def legal_moves(self):
        return [ (x, y) for x in range(9) for y in range(9) if self.is_legal(x, y) ]
        
    def make_move(self, x, y, pid):
        mbx, mby = x/3, y/3
        self.macroboard[3*mby+mbx] = -1
        self.board[9*y+x] = pid

    def is_fatal(self, x, y, myid, oppid, move_score):
        # 0 = opp can't win any macro, 1 = opp can win macro, 2 = opp can win whole game
        store_macro = list(self.macroboard)
        store_board = list(self.board)
        # self.macroboard[3*mby+mbx] = -1
        if move_score > 0:
            self.macroboard[3*mby+mbx] = myid
        move_loc = 9*y+x
        self.board[move_loc] = myid
        self.update_macro(x,y)

        # now analyze opponents moves
        max_move = 0
        lmoves = self.legal_moves()
        for move in lmoves:
            move_score = self.can_win_macro(move[0], move[1], oppid)
            if move_score > max_move:
                max_move = move_score

        # reset
        self.macroboard = store_macro
        self.board = store_board
        return max_move

    def update_macro(self, x, y):
        mbx, mby = x/3, y/3
        macro_num = 3*mby+mbx
        start = self.get_macro_start(macro_num)
        move_loc = 9*y+x
        diff = move_loc - start
        if diff in [0,1,2]:
            diff_pos = diff
        elif diff in [9,10,11]:
            diff_pos = diff - 6
        elif diff in [18,19,20]:
            diff_pos = diff - 12
        # else:
        #     raise ValueError(diff)

        if self.macroboard[diff_pos] <= 0:
            for i in range(len(self.macroboard)):
                if self.macroboard[i] <= 0:
                    self.macroboard[i] = 0
            self.macroboard[diff_pos] = -1
        else:
            for i in range(len(self.macroboard)):
                if self.macroboard[i] <= 0:
                    self.macroboard[i] = -1

    def get_macro_start(self, macro):
        if macro == 0:
            return 0
        elif macro == 1:
            return 3
        elif macro == 2:
            return 6
        elif macro == 3:
            return 27
        elif macro == 4:
            return 30
        elif macro == 5:
            return 33
        elif macro == 6:
            return 54
        elif macro == 7:
            return 57
        else:
            return 60

    def can_win_macro(self, x, y, pid):
        # 0 = can't win, 1 = win individual macro, 2 = win whole game
        mbx, mby = x/3, y/3
        macro_num = 3*mby+mbx
        start = self.get_macro_start(macro_num)
        move_pos = 9*y+x
        store = self.board[move_pos]
        self.board[move_pos] = pid
        goodness = 0
        # raise ValueError(str(move_pos))
        # top row
        if(self.board[start] == pid and self.board[start]==self.board[start+1] and self.board[start]==self.board[start+2]):
            goodness = 1
        # second row
        elif(self.board[start+9] == pid and self.board[start+9]==self.board[start+10] and self.board[start+9]==self.board[start+11]):
            goodness = 1
        # bottom row
        elif(self.board[start+18] == pid and self.board[start+18]==self.board[start+19] and self.board[start+18]==self.board[start+20]):
            goodness = 1
        # first column
        elif(self.board[start] == pid and self.board[start]==self.board[start+9] and self.board[start]==self.board[start+18]):
            goodness = 1
        # second column
        elif(self.board[start+1] == pid and self.board[start+1]==self.board[start+10] and self.board[start+1]==self.board[start+19]):
            goodness = 1
        # third column
        elif(self.board[start+2] == pid and self.board[start+2]==self.board[start+11] and self.board[start+2]==self.board[start+20]):
            goodness = 1
        # diagonal \
        elif(self.board[start] == pid and self.board[start]==self.board[start+10] and self.board[start]==self.board[start+20]):
            goodness = 1
        # diagonal /
        elif(self.board[start+2] == pid and self.board[start+2]==self.board[start+10] and self.board[start+2]==self.board[start+18]):
            goodness = 1

        if goodness == 1 and self.can_win_whole_game(macro_num, pid):
            goodness = 2

        # reset board
        self.board[move_pos] = store
        return goodness

    def can_win_whole_game(self, macro_num, pid):
        store = self.macroboard[macro_num]
        self.macroboard[macro_num] = pid
        is_win = False
        # top row
        if(self.macroboard[0] == pid and self.macroboard[1] == pid and self.macroboard[2] == pid):
            is_win = True
        # second row
        elif(self.macroboard[3] == pid and self.macroboard[4] == pid and self.macroboard[5] == pid):
            is_win = True
        # third row
        elif(self.macroboard[6] == pid and self.macroboard[7] == pid and self.macroboard[8] == pid):
            is_win = True
        # first column
        elif(self.macroboard[0] == pid and self.macroboard[3] == pid and self.macroboard[6] == pid):
            is_win = True
        # second column
        elif(self.macroboard[1] == pid and self.macroboard[4] == pid and self.macroboard[7] == pid):
            is_win = True
        # third column
        elif(self.macroboard[2] == pid and self.macroboard[5] == pid and self.macroboard[8] == pid):
            is_win = True
        # diagonal \
        elif(self.macroboard[0] == pid and self.macroboard[4] == pid and self.macroboard[8] == pid):
            is_win = True
        # diagonal /
        elif(self.macroboard[2] == pid and self.macroboard[4] == pid and self.macroboard[6] == pid):
            is_win = True

        # reset macroboard
        self.macroboard[macro_num] = store
        return is_win

    # def is_fatal(self, x, y, myid, oppid):
    #     # 0 = opp can't win any macro, 1 = opp can win macro, 2 = opp can win whole game
    #     move_pos = 9*y+x
    #     store = self.board[move_pos]

    #     return 0

    def get_board(self):
        return ''.join(self.board, ',')
        # return ','.join(self.board)


    def get_macroboard(self):
        return ''.join(self.macroboard, ',')
        # return ','.join(self.macroboard)
    