from random import randint
from position import Position

class RandomBot:

	def __init__(self):
		self.myid = None
		self.oppid = None

	def get_move(self, pos, tleft):
		lmoves = pos.legal_moves()
		winning_moves = []
		block_fatal_moves = []
		blocking_moves = []
		for move in lmoves:
			move_score = pos.can_win_macro(move[0], move[1], self.myid)
			their_score = pos.can_win_macro(move[0], move[1], self.oppid)
			if move_score == 2:
				return move
			elif move_score == 1:
				winning_moves.append(move)
			if their_score == 2:
				block_fatal_moves.append(move)
			elif their_score == 1:
				blocking_moves.append(move)
			

		if(len(winning_moves) > 0):
			rg = randint(0, len(winning_moves)-1)
			return winning_moves[rg]
		elif(len(block_fatal_moves) > 0):
			rg = randint(0, len(block_fatal_moves)-1)
			return block_fatal_moves[rg]
		elif(len(blocking_moves) > 0):
			rg = randint(0, len(blocking_moves)-1)
			return blocking_moves[rg]

		# if don't find a better move, return a random move
		rm = randint(0, len(lmoves)-1)
		return lmoves[rm]
		# return (100,100)

